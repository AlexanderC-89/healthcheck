package checker

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
	. "gitlab.com/rakshazi/healthcheck/monitor"
)

// Redis check
func checkRedis(URLMonitor *URLMonitor) (int, int, error) {
	var start time.Time
	var ttfb int
	config, err := redis.ParseURL(URLMonitor.URL)
	if err != nil {
		return 0, 0, fmt.Errorf("cannot fetch monitor %s: %v", URLMonitor.Name, err)
	}
	start = time.Now()
	client := redis.NewClient(config)
	_, err = client.Ping().Result()
	if err != nil {
		return 0, ttfb, fmt.Errorf("cannot fetch monitor %s: %v", URLMonitor.Name, err)
	}
	ttfb = int(time.Since(start) / time.Millisecond)
	return 200, ttfb, nil
}
