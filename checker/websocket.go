package checker

import (
	"fmt"
	"net/url"
	"time"

	. "gitlab.com/rakshazi/healthcheck/monitor"
	"golang.org/x/net/websocket"
)

// Websocket check
func checkWebsocket(URLMonitor *URLMonitor) (int, int, error) {
	origin, _ := url.Parse(URLMonitor.URL)
	var ttfb int
	var start time.Time = time.Now()
	ws, err := websocket.Dial(URLMonitor.URL, "", origin.Scheme+"://"+origin.Host)
	if err != nil {
		return 0, 0, fmt.Errorf("cannot fetch monitor %s: %v", URLMonitor.Name, err)
	}
	ttfb = int(time.Since(start) / time.Millisecond)
	if err = ws.Close(); err != nil {
		return 200, ttfb, err
	}

	return 200, ttfb, nil
}
