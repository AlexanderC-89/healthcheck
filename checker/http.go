package checker

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptrace"
	"time"

	. "gitlab.com/rakshazi/healthcheck/monitor"
)

// HTTP(-s) check
func checkHttp(monitor *URLMonitor, timeout time.Duration) (int, int, error) {
	var start time.Time = time.Now()
	var ttfb int
	req, _ := http.NewRequest(http.MethodGet, monitor.URL, nil)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	trace := &httptrace.ClientTrace{
		GotFirstResponseByte: func() {
			ttfb = int(time.Since(start) / time.Millisecond)
		},
	}
	req = req.WithContext(httptrace.WithClientTrace(ctx, trace))
	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		return 0, ttfb, fmt.Errorf("cannot fetch monitor %s: %v", monitor.Name, err)
	}

	return resp.StatusCode, ttfb, nil
}
