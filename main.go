package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"

	. "gitlab.com/rakshazi/healthcheck/checker"
	"gitlab.com/rakshazi/healthcheck/config"
	"gitlab.com/rakshazi/healthcheck/listener"
	"gitlab.com/rakshazi/healthcheck/monitor"
)

func main() {
	configFile := flag.String("config", "config.json", "Path to the configuration file")

	flag.Parse()

	config, err := config.CreateConfigurationFromFile(*configFile)

	if err != nil {
		log.Fatalf("Could not load file: %v", err)
	}

	checker := Checker{Config: config}

	// Register slack listener
	if config.Slack.Enabled {
		err = checker.RegisterListener(&listener.SlackListener{Config: config.Slack})

		if err != nil {
			// We continue to monitor, even if we can't register slack listener
			log.Printf("Warning: %v", err)
		}
	}

	// Register telegram listener
	if config.Telegram.Enabled {
		err = checker.RegisterListener(&listener.TelegramListener{Config: config.Telegram})

		if err != nil {
			// We continue to monitor, even if we can't register listener
			log.Printf("Warning: %v", err)
		}
	}

	// Register twilio sms listener
	if config.Twilio.SMS.Enabled {
		err = checker.RegisterListener(&listener.TwilioSMSListener{
			Config: config.Twilio.SMS,
		})

		if err != nil {
			// We continue to monitor, even if we can't register twilio sms listener
			log.Printf("Warning: %v", err)
		}
	}

	for _, provider := range config.URLMonitors {
		err = checker.RegisterProvider(&monitor.URLMonitor{
			Name: provider.Name,
			URL:  provider.URL,
		})

		if err != nil {
			log.Fatalf("Error registering monitor: %v", err)
		}

		log.Printf("Provider %s registered", provider.Name)
	}

	statusHandler := StatusHandler{Checker: &checker}
	http.HandleFunc(config.Server.Endpoint, statusHandler.Handle)

	go checker.Run()

	port := fmt.Sprintf(":%d", config.Server.Port)
	log.Printf("Listening on port %d...", config.Server.Port)
	log.Fatal(http.ListenAndServe(port, nil))
}

type Response struct {
	Providers []*monitor.URLMonitor `json:"providers"`
}

type StatusHandler struct {
	Checker *Checker
}

func (sh *StatusHandler) Handle(w http.ResponseWriter, r *http.Request) {
	var code int = 200
	response := Response{
		Providers: sh.Checker.URLMonitors,
	}
	for _, monitor := range sh.Checker.URLMonitors {
		status, _, _, _ := monitor.GetCurrentStatus()
		if status != true {
			code = 503
		}
	}

	jsonResponse, _ := json.Marshal(response)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if _, err := w.Write(jsonResponse); err != nil {
		log.Printf("Warning: %v", err)
	}
}
