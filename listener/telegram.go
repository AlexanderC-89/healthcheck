package listener

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/rakshazi/healthcheck/monitor"

	"gitlab.com/rakshazi/healthcheck/config"
)

// TelegramListener Will send a telegram notification when a monitor status changes
type TelegramListener struct {
	Config config.TelegramConfig
}

func (tg *TelegramListener) OnRegister() error {
	if tg.Config.Token == "" || tg.Config.ChatId == 0 {
		return fmt.Errorf("telegram's bot token or chat id is empty")
	}

	return nil
}

func (tg *TelegramListener) OnStatusChange(m *monitor.URLMonitor) error {
	message := tg.Config.Messages.Healthy

	status, _, _, _ := m.GetCurrentStatus()

	if !status {
		message = tg.Config.Messages.Unhealthy
	}

	message, err := parseMessage(message, *m)

	telegramMessage := telegramMessage{
		ChatId: tg.Config.ChatId,
		Text:   message,
	}

	payload, err := json.Marshal(telegramMessage)

	if err != nil {
		return fmt.Errorf("could not marshall struct: %v", err)
	}

	url := "https://api.telegram.org/bot" + tg.Config.Token + "/sendMessage"
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)

	if err != nil {
		return fmt.Errorf("could not make request to telegram: %v", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf("bad response from telegram: %v", resp.Status)
	}

	return nil
}

type telegramMessage struct {
	ChatId int64  `json:"chat_id"`
	Text   string `json:"text"`
}
