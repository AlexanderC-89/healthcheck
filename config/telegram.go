package config

type TelegramConfig struct {
	Enabled  bool
	Token    string
	ChatId   int64
	Messages TelegramMessagesConfig
}

type TelegramMessagesConfig struct {
	Healthy   string
	Unhealthy string
}
